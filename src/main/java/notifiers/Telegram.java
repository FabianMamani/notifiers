package notifiers;

import discovery.Notifier;

public class Telegram implements Notifier {

    @Override
    public String send() {
        String msj = "Notificación enviada por notifiers.Telegram.";
        return msj;
    }
}
