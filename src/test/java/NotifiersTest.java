import channelNotification.Whastapp;
import discovery.Notifier;
import org.junit.Assert;
import org.junit.Test;


public class NotifiersTest {

    @Test
    public void notifiersTelegram(){
        Notifier notificator = new Telegram();
        Assert.assertEquals("Notificación enviada por Telegram.",notificator.send());

    }

    @Test
    public void notifiersWhatsapp(){
        Notifier notificator = new Whastapp();
        Assert.assertEquals("Notificación enviada por Whatsapp.",notificator.send());
    }
}
